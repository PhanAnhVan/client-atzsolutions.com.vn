function scrolltop() {
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 200) {
            $('.scrollTop').fadeIn(200);
        } else {
            $('.scrollTop').fadeOut(200);
        }
    });
    $('.scrollTop').on('click', function () {
        $('html,body').animate({ scrollTop: 0 }, 1500);
        return false;
    });
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle=popover]').popover();
});

// var _Hasync = _Hasync || [];
// function histats() {
//     _Hasync.push(['Histats.start', '1,4562117,4,429,112,75,00010111']);
//     _Hasync.push(['Histats.fasi', '1']);
//     _Hasync.push(['Histats.track_hits', '']);
//     (function () {
//         var hs = document.createElement('script');
//         hs.type = 'text/javascript';
//         hs.async = true;
//         hs.src = '//s10.histats.com/js15_as.js';
//         (
//             document.getElementsByTagName('head')[0] ||
//             document.getElementsByTagName('body')[0]
//         ).appendChild(hs);
//     })();

// setTimeout(() => {
//     let img = document.querySelectorAll('img');

//     img[img.length - 1].remove();
//     img[img.length - 2].remove();
// }, 3300); 
// remove 2 image residual after histats render
// }

// function replaceFancybox() {
//     setTimeout(function () {
//         $('#listSocial .fancybox').replaceWith(function () {
//             return $('<div>', {
//                 html: this.innerHTML,
//             });
//         });
//     }, 2000);
// }

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

export class configOptions {
    path: string;
    data?: Object;
    params?: Object;
    token: string;
}
@Injectable()
export class Globals {
    public BASE_API_URL = 'https://atzsolutions.com.vn/';

    public admin: string = 'admin';
    public company: any = {};
    private response = new Subject<any>();
    public result = this.response.asObservable();

    public configCkeditor = {
        filebrowserBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl:
            this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl:
            this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl:
            this.BASE_API_URL +
            'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl:
            this.BASE_API_URL +
            'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl:
            this.BASE_API_URL +
            'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
    };

    constructor(private http: HttpClient, public router: Router) { }

    send = (option: configOptions) => {
        // check token
        if (option.path && option.token) {
            let params = () => {
                let param = '?mask=' + option.token;
                let link = window.location.pathname.split('/')[1] || '';
                let language =
                    link === this.admin
                        ? +this.language.get(true)
                        : +this.language.get(false);
                param += '&language=' + language;
                if (option.params) {
                    let keys = Object.keys(option.params);
                    for (let i = 0; i < keys.length; i++) {
                        if (i == 0) {
                            param += '&';
                        }
                        param += keys[i] + '=' + option.params[keys[i]];
                        param += i + 1 == keys.length ? '' : '&';
                    }
                }
                return param;
            };
            this.http
                .post(this.BASE_API_URL + option.path + params(), option.data)
                .subscribe((result: any) => {
                    this.response.next(result);
                });
        }
    };

    public time = {
        format: e => {
            e = typeof e === 'object' ? e : new Date();
            return (
                e.getFullYear() +
                '-' +
                (e.getMonth() + 1).toString() +
                '-' +
                e.getDate()
            );
        },
        date: e => {
            e = typeof e === 'object' ? e : new Date();
            return (
                e.getFullYear() +
                '-' +
                (e.getMonth() + 1).toString() +
                '-' +
                e.getDate()
            );
        },
    };

    public USERS = {
        token: 'users',
        store: 'localStorage',
        get: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.USERS.store)
                    ? JSON.parse(window.localStorage.getItem(this.USERS.store))
                    : {};
            } else {
                return window.localStorage.getItem(this.USERS.token)
                    ? window.localStorage.getItem(this.USERS.token)
                    : null;
            }
        },
        check: skip => {
            return skip == true
                ? window.localStorage.getItem(this.USERS.store)
                    ? true
                    : false
                : window.localStorage.getItem(this.USERS.token)
                    ? true
                    : false;
        },
        set: (data, skip) => {
            data = typeof data === 'object' ? JSON.stringify(data) : data;
            if (skip == true) {
                window.localStorage.setItem(this.USERS.store, data);
            } else {
                window.localStorage.setItem(this.USERS.token, data);
            }
        },
        remove: (skip: any = '') => {
            if (!skip) {
                window.localStorage.clear();
            } else {
                if (skip == true) {
                    window.localStorage.removeItem(this.USERS.store);
                } else {
                    window.localStorage.removeItem(this.USERS.token);
                }
            }
            this.router.navigate(['/login/']);
            return (
                this.http
                    .post(this.BASE_API_URL + 'api/logout/admin', {})
                    // .map((response: Response) => response.json())
                    .subscribe((result: any) => {
                        this.response.next(result);
                    })
            );
        },
        _check: () => {
            return this.http.post(
                this.BASE_API_URL + 'api/login/check',
                this.USERS.get(true)
            );
        },
    };

    public language = {
        skip: true,
        token: 'language', // number
        tokenCode: 'languageCode', // string
        tokenAdmin: 'languageAdmin', // number
        store: 'localStoragelanguage', // object
        get: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.language.tokenAdmin)
                    ? window.localStorage.getItem(this.language.tokenAdmin)
                    : 0;
            } else {
                return window.localStorage.getItem(this.language.token)
                    ? window.localStorage.getItem(this.language.token)
                    : 0;
            }
        },
        getCode: () => {
            return window.localStorage.getItem(this.language.tokenCode)
                ? window.localStorage.getItem(this.language.tokenCode)
                : 0;
        },
        setCode: item => {
            item = typeof item === 'object' ? JSON.stringify(item) : item;
            window.localStorage.setItem(this.language.tokenCode, item);
        },
        set: (item, skip) => {
            item = typeof item === 'object' ? JSON.stringify(item) : item;
            if (skip == true) {
                window.localStorage.setItem(this.language.tokenAdmin, item);
            } else {
                window.localStorage.setItem(this.language.token, item);
            }
        },
        check: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.language.tokenAdmin)
                    ? true
                    : false;
            } else {
                return window.localStorage.getItem(this.language.token) ? true : false;
            }
        },
        getData: () => {
            return window.localStorage.getItem(this.language.store)
                ? JSON.parse(window.localStorage.getItem(this.language.store))
                : {};
        },
        setData: data => {
            data = typeof data === 'object' ? JSON.stringify(data) : data;
            window.localStorage.setItem(this.language.store, data);
        },
        process: () => {
            let data = JSON.parse(window.localStorage.getItem(this.language.store));
            let link = window.location.pathname.split('/')[1] || '';
            let language = this.language.get(false) || 0;
            let router = '';
            let active: any = {};
            if (link != '') {
                data.filter(item => {
                    item.code == link ? (active = item) : {};
                    return item;
                });
                router = window.location.pathname;
            } else {
                data.filter(item => {
                    +item.id == +language ? (active = item) : {};
                    return item;
                });
                router = '/' + active.code;
            }
            if (Object.keys(active).length == 0) {
                data.filter(item => {
                    +item.defaults == 0 ? (active = item) : {};
                    return item;
                });
                router = '/' + active.code;
            }
            let format = /[%]/;
            this.language.set(active.id, false);

            this.language.setCode(active.code);
            link == this.admin || link == 'login' || format.test(router) ? '' : this.router.navigate([router]);
        },
    };

    initHeader = {
        state: false,
        isActive: 0,
        isFixed: false,
        isActiveLink: '',
        changeState: (url: string) => {
            let language: string

            if (!this.language.getCode()) {
                language = '/en'
            } else {
                language = '/' + this.language.getCode()
            }

            url !== language ? this.initHeader.state = true : this.initHeader.state = false

            if (!this.initHeader.state) {
                this.initHeader.isActiveLink = ''
            }
        }
    }
}

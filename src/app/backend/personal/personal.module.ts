import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule, AlertModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { GetlistComponent } from './getlist/getlist.component';

import { ChangespasswordComponent } from './changespassword/changespassword.component';

import { uploadFileService } from '../../services/integrated/upload.service';
import { ProcessComponent } from './process/process.component';



const appRoutes: Routes = [
	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: GetlistComponent },
	{ path: 'insert', component: ProcessComponent },
	{ path: 'update/:id', component: ProcessComponent },
	{ path: 'changespassword/:id', component: ChangespasswordComponent },
]

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		ModalModule.forRoot(),
		AlertModule.forRoot(),
		TranslateModule,
		BsDatepickerModule.forRoot(),
	],
	providers: [uploadFileService],
	declarations: [
		GetlistComponent,
		ProcessComponent,
		ChangespasswordComponent,

	]
})
export class PersonalModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';

import { GetlistComponent } from './getlist/getlist.component';
import { InfoContactComponent } from './info-contact/info-contact.component';

const contactRoute: Routes = [

    { path: '', redirectTo: 'get-list' },

    { path: 'get-list', component: GetlistComponent },

    { path: 'info-contact/:id', component: InfoContactComponent },
]
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(contactRoute),
        ModalModule.forRoot(),
        TranslateModule
    ],
    declarations: [
        GetlistComponent,
        InfoContactComponent,
    ],
    providers: []
})
export class ContactModule { }

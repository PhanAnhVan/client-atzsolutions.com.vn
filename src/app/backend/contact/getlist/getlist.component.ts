import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Globals } from '../../../globals';
import { AlertComponent } from '../../modules/alert/alert.component';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
})

export class GetlistComponent implements OnInit, OnDestroy {

    private modalRef: BsModalRef;

    public data: any;

    public id: number;

    public connect;

    private cols = [

        { title: 'lblStt', field: 'index', show: true, filter: true },

        { title: 'contact.email', field: 'email', show: true, filter: true },

        { title: 'contact.subject', field: 'subject', show: true, filter: true },

        { title: 'contact.message', field: 'message', show: true, filter: true },

        { title: 'contact.maker_date', field: 'maker_date', show: true, filter: true },

        { title: 'contact.id_checked', field: 'user_name', show: true, filter: true },

        { title: '', field: 'checked', show: true, filter: true },

    ];

    public token: any = {
        getlist: "get/contact/getlist"
    }

    constructor(

        public cwstable: TableService,

        public modalService: BsModalService,

        public toastr: ToastrService,

        public router: Router,

        public globals: Globals,

        public translate: TranslateService,

    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getlist":
                    this.cwstable._concat(res.data, true);
                    break;

                case "remove":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type);
                    if (res.status == 1) {
                        this.cwstable._delRowData(this.id)
                    }
                    break;

                default:
                    break;
            }
        });




    }

    ngOnInit() {

        this.globals.send({ path: this.token.getlist, token: 'getlist' });

        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'contact', sorting: { field: "id", sort: "DESC", type: "" } });

    }

    ngOnDestroy() {

        this.connect.unsubscribe();

    }

    onRemove(id: number, name: any) {

        this.id = id;

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'contact.mesRemoveContact', name: name } });

        this.modalRef.content.onClose.subscribe(result => {

            if (result == true) {

                this.globals.send({ path: this.token.remove, token: 'remove', params: { id: id } });
            }
        });
    }
}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ModalModule } from "ngx-bootstrap/modal";
import { AlertModule } from "ngx-bootstrap/alert";

import { GetlistComponent } from "./getlist/getlist.component";

import { ProcessComponent } from "./process/process.component";
import { MainComponent } from "./main/main.component";
import { GroupgetlistComponent } from "./groupgetlist/groupgetlist.component";

const appRoutes: Routes = [
  { path: "", redirectTo: "get-list" },
  { path: "get-list", component: MainComponent },
  { path: "insert", component: ProcessComponent },
  { path: "update/:id", component: ProcessComponent },
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(appRoutes),
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    TranslateModule,
  ],
  declarations: [
    GetlistComponent,
    ProcessComponent,
    MainComponent,
    GroupgetlistComponent,
  ],
})
export class LibraryModule {}

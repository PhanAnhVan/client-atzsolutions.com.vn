import { NgModule } from '@angular/core';
import { CKEditorModule } from 'ckeditor4-angular';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule, AlertModule, TabsModule, TooltipModule, BsModalRef, TypeaheadModule } from 'ngx-bootstrap';

import { GetlistComponent } from './getlist/getlist.component';
import { GroupProductGetlistComponent } from './group/group-product.component';
import { MainComponent } from './main/main.component';
import { ProcessProductComponent } from './process/process.component';
import { NumberDirective } from '../../services/directive/number.directive';

const appRoutes: Routes = [

    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: MainComponent },
    { path: 'insert', component: ProcessProductComponent },
    { path: 'update/:id', component: ProcessProductComponent },
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(appRoutes),
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        TranslateModule,
        CKEditorModule,
        TypeaheadModule.forRoot()
    ],
    providers: [
        BsModalRef,

    ],
    declarations: [
        GetlistComponent,
        GroupProductGetlistComponent,
        ProcessProductComponent,
        MainComponent,
        NumberDirective
    ],
})

export class ProductModule { }

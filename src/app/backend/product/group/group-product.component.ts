import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewContainerRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TableService } from '../../../services/integrated/table.service';
import { Globals } from '../../../globals';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-group-product',
    templateUrl: './group-product.component.html',
    styleUrls: ['./group-product.component.css']
})

/**
 * diem.ntn
 * create: 14/2/2020
 */

export class GroupProductGetlistComponent implements OnInit, OnDestroy {

    public show;
    public connect;

    public id;

    @Output("filter") filter = new EventEmitter();

    public token: any = {
        getlist: "get/pages/grouptype"
    }

    modalRef: BsModalRef;

    private cols = [

        { title: 'lblGroup', field: 'name', show: true, filter: true },

        { title: 'lblCount', field: 'count', show: true, filter: true },
    ];
    public tablegroupproduct = new TableService();

    constructor(

        public globals: Globals,

    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistproductgroup":
                    this.tablegroupproduct._concat(res.data, true);
                    break;

                default:
                    break;
            }
        });
    }
    ngOnInit() {
        this.getlist()
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    getlist = () => {
        this.globals.send({ path: this.token.getlist, token: 'getlistproductgroup', params: { type: 3 } });
        this.tablegroupproduct._ini({ cols: this.cols, data: [] });
    }

    onCheckItem(item) {
        item.check = (item.check == true) ? false : true;
        this.filter.emit(item.id);
    }

}

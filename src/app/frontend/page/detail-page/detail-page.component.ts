import { ToslugService } from './../../../services/integrated/toslug.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from 'app/globals';

@Component({
    selector: 'app-detail-page',
    templateUrl: './detail-page.component.html',
    styleUrls: ['./detail-page.component.scss'],
    providers: [ToslugService],
})
export class DetailPageComponent implements OnInit, OnDestroy {
    public connect;
    public data = <any>{};
    public link: string = '';
    public width;
    public page = <any>{};
    public token: any = {
        getDetail: 'api/page/detail',
    };

    constructor(
        public globals: Globals,
        public fb: FormBuilder,
        public route: ActivatedRoute,
        public router: Router,
        public toSlug: ToslugService
    ) {
        this.width = window.innerWidth;
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getDetail':
                    this.data = res.data;
                    this.data.keywords = JSON.parse(this.data.keywords);

                    setTimeout(() => {
                        this.renderHtml();
                    }, 500);
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            setTimeout(() => {
                this.globals.send({
                    path: this.token.getDetail,
                    token: 'getDetail',
                    params: {
                        link: params.link,
                        parent_link: params.parent_link,
                    },
                });
            }, 50);
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    renderHtml = () => {
        let main = document.getElementById('contentDetail');
        if (main) {
            let el = main.querySelectorAll('table');
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement('div');
                    div.className = 'table-responsive table-bordered m-0 border-0';
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i];
                    let newhtml = "<table class='table'>" + el[i].innerHTML + '</table>';
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
        }
    };

    search = {
        value: '',
        params: <any>{},
        onSearch: (value: string) => {
            this.search.value = this.toSlug._ini(value.replace(/\s+/g, ' ').trim());
            if (this.search.check_search(this.search.value)) {
                this.search.params.value = this.search.value;

                let language = this.globals.language.getCode();
                this.router.navigate([`/${language}/search/${this.search.value}`]);
            }
            this.search.value = '';
        },
        check_search: (value: string) => {
            let skip = true;
            skip = value.toString().length > 0 &&
                value != '' &&
                value != '-' &&
                value != '[' &&
                value != ']' &&
                value != '\\' &&
                value != '{' &&
                value != '}'
                ? true
                : false;
            return skip;
        }
    }
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer, Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.scss'],
})
export class ListPageComponent implements OnInit, OnDestroy {
    private connect;
    public width: number = window.innerWidth;
    public pageView: number = -1;
    public language: string = this.globals.language.getCode() + '/' || '';

    activeCode: boolean = false;

    public token: any = {
        getPage: 'api/page/getpageByLink',
        contents: 'api/page/contents'
    };

    constructor(
        public globals: Globals,
        public route: ActivatedRoute,
        public router: Router,
        public sanitizer: DomSanitizer,
        private title: Title,
        private meta: Meta
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getPage':
                    this.data = res.data;
                    this.title.setTitle(this.data.name);
                    this.meta.updateTag({
                        name: 'description',
                        content: this.data.description,
                    });

                    // window['replaceFancybox'] ? window['replaceFancybox']() : '';

                    switch (this.data.code) {
                        case 'profile':
                            this.data.list_image = JSON.parse(this.data.list_image);
                            break;

                        case 'about':
                            // Company -> Only show item pin === 0
                            this.data.data = this.data.data.filter(
                                (_res: any) => +_res.pin === 0
                            );
                            break;

                        default:
                            break;
                    }

                    if (Object.keys(this.data).length > 0) {
                        this.activeCode = this._checkCodeMenu(this.data.codepage);

                        let type = res.data.type || 0;

                        switch (+type) {
                            case 2:
                            case 3:
                            case 5:
                                setTimeout(() => {
                                    this.pageView = Object.keys(this.data).length > 0 ? 1 : 0;
                                }, 100);
                                break;
                            case 4:
                                this.contents.send(res.data.id);
                                break;

                            default:
                                break;
                        }
                    }

                    setTimeout(() => {
                        this.renderHtml();
                    }, 500);
                    break;

                case 'getDataContents':
                    this.contents.data = res.data.data.slice(
                        0,
                        this.Pagination.itemsPerPage
                    );

                    this.contents.cached = res.data.data;

                    this.contents.group = res.data.group;

                    setTimeout(() => {
                        this.pageView = this.contents.data.length > 0 ? 1 : 0;
                    }, 100);
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            let link = params.link || '';

            this.globals.send({
                path: this.token.getPage,
                token: 'getPage',
                params: { link: link },
            });
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    _checkCodeMenu(code = '') {
        let skip = false;

        switch (code) {
            case 'products':
            case 'solution':
            case 'services':
            case 'resources':
            case 'company':
                skip = true;
                break;

            default:
                break;
        }

        return skip;
    }

    public data: any = {
        leftAbout: [],
        rightAbout: [],
        send: (id) => {
            this.globals.send({
                path: this.token.getPage,
                token: 'getPage',
                params: { id: id || 0 },
            });
        },
    };

    options: OwlOptions = {
        loop: false,
        rewind: true,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 3300,
        dots: false,
        nav: false,
        items: this.width >= 1025 ? 3 : this.width >= 426 ? 2 : 1,
        margin: this.width >= 1025 ? 30 : 20,
        stagePadding: this.width >= 426 ? 1 : 30,
    };

    public contents = {
        group: [],
        data: [],
        cached: [],
        send: (id) => {
            this.globals.send({
                path: this.token.contents,
                token: 'getDataContents',
                params: { id: id || 0 },
            });
        },
    };

    public Pagination = {
        maxSize: 5,
        itemsPerPage: 10,
        change: (event: PageChangedEvent) => {
            const itemStart = (event.page - 1) * event.itemsPerPage;

            const itemEnd = event.page * event.itemsPerPage;

            this.contents.data = this.contents.cached.slice(itemStart, itemEnd);

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth',
            });
        },
    };

    profile = {
        options: {
            loop: false,
            autoplaySpeed: 3500,
            autoplay: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            nav: true,
            dots: false,
            // slideBy: 2,
            navText: [
                "<img src='../../../../../assets/img/page-back-icon.png' width='64' />",
                "<img src='../../../../../assets/img/page-next-icon.png' width='64' />",
            ],
            items: 1,
        },
    };

    renderHtml = () => {
        let main = document.getElementById('contentDetail');
        if (main) {
            let el = main.querySelectorAll('table');
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement('div');
                    div.className = 'table-responsive table-bordered m-0 border-0';
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i];
                    let newhtml = "<table class='table'>" + el[i].innerHTML + '</table>';
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
        }
    };
}

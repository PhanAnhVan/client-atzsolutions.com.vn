import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Globals } from 'app/globals';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit, OnDestroy {
    public connect;
    fm: FormGroup;
    public company: any;
    public flags: boolean = true;
    public width: number;

    private token = {
        addContact: 'api/addContact',
        getPage: 'api/page/getpageByLink'
    }
    constructor(
        public globals: Globals,
        public fb: FormBuilder,
        public toastr: ToastrService,
        public route: ActivatedRoute,
        private title: Title,
        private meta: Meta
    ) {
        this.width = window.innerWidth;
        this.contact.fmConfigs();
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'addContact':
                    let type = res['status'] == 1 ? 'success' : res['status'] == 0 ? 'warning' : 'danger';
                    this.toastr[type](res['message'], type);
                    res.status == 1 ? this.contact.fmConfigs() : !1;
                    break;
                case 'getPage':
                    this.contact.data = res.data
                    this.title.setTitle(this.contact.data.name)
                    this.meta.updateTag({
                        name: 'description',
                        content: this.contact.data.description
                    })
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe((params) => {
            let link = params.lang == 'vn' ? 'lien-he' : 'contact';
            this.globals.send({
                path: this.token.getPage,
                token: 'getPage',
                params: { link: link },
            });
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    public contact = {
        data: <any>{},
        token: 'api/addContact',
        fmConfigs: (item: any = '') => {
            item = typeof item === 'object' ? item : {};
            this.fm = this.fb.group({
                name: ['', [Validators.required]],
                email: [
                    '',
                    [
                        Validators.required,
                        Validators.pattern(
                            /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
                        ),
                    ],
                ],
                phone: [
                    '',
                    [
                        Validators.required,
                        Validators.pattern(
                            /^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/
                        ),
                    ],
                ],
                subject: ['', [Validators.required]],
                message: ['', [Validators.required]],
            });
        },
        addContact: () => {
            if (this.fm.valid) {
                this.globals.send({
                    path: this.contact.token,
                    token: 'addContact',
                    data: this.fm.value
                })
            }
        },
    }
}

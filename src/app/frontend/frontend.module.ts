import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import {
    intersectionObserverPreset,
    LazyLoadImageModule,
} from 'ng-lazyload-image';
import { Ng5SliderModule } from 'ng5-slider';
import {
    BsDatepickerModule,
    PaginationModule,
    TabsModule,
    TimepickerModule,
} from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { CarouselModule as OwlCarouselModule } from 'ngx-owl-carousel-o';
import { BindSrcDirective } from '../services/directive/bindSrc.directive';
import { ContactComponent } from './contact/contact.component';
import { FrontendComponent } from './frontend.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HomeComponent } from './home/home.component';
import { BoxContentGridComponent } from './modules/box-content-grid/box-content-grid.component';
import { BoxContentHorizontalComponent } from './modules/box-content-horizontal/box-content-horizontal.component';
import { BoxContentComponent } from './modules/box-content/box-content.component';
import { BoxGalleryComponent } from './modules/box-gallery/box-gallery.component';
import { BoxProjectComponent } from './modules/box-project/box-project.component';
import { FooterComponent } from './modules/footer/footer.component';
import { HeaderComponent } from './modules/header/header.component';
import { MenuMobileComponent } from './modules/menu-mobile/menu-mobile.component';
import { MenuComponent } from './modules/menu/menu.component';
import { DetailPageComponent } from './page/detail-page/detail-page.component';
import { ListPageComponent } from './page/list/page.component';
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe';
import { SearchComponent } from './search/search.component';

const appRoutes: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: [
            { path: ':lang/home', redirectTo: '' },
            { path: ':lang/trang-chu', redirectTo: '' },
            { path: ':lang', component: HomeComponent },

            { path: ':lang/lien-he', component: ContactComponent },
            { path: ':lang/contact', component: ContactComponent },

            { path: ':lang/search', component: SearchComponent },
            { path: ':lang/search/:keywords', component: SearchComponent },

            { path: ':lang/:link', component: ListPageComponent },
            { path: ':lang/:parent_link/:link', component: ListPageComponent },
            {
                path: ':lang/:parent_links/:parent_link/:link',
                component: DetailPageComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        TabsModule.forRoot(),
        RouterModule.forChild(appRoutes),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ModalModule.forRoot(),
        OwlCarouselModule,
        TypeaheadModule.forRoot(),
        PaginationModule.forRoot(),
        LazyLoadImageModule.forRoot({
            preset: intersectionObserverPreset,
        }),
        Ng5SliderModule,
    ],
    declarations: [
        SanitizeHtmlPipe,
        BindSrcDirective,
        FrontendComponent,
        HomeComponent,
        HeaderComponent,
        FooterComponent,
        MenuComponent,
        MenuMobileComponent,
        ListPageComponent,
        DetailPageComponent,
        SearchComponent,
        ContactComponent,
        BoxContentComponent,
        BoxContentHorizontalComponent,
        BoxProjectComponent,
        BoxContentGridComponent,
        GalleryComponent,
        BoxGalleryComponent,
    ],
})
export class FrontendModule { }

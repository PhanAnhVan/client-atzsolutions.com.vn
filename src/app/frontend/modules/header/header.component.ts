import { Component, EventEmitter, OnChanges, OnInit, Output } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Globals } from 'app/globals';
import { ToslugService } from 'app/services/integrated/toslug.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    providers: [ToslugService]
})
export class HeaderComponent implements OnInit {
    @Output("loading") loading = new EventEmitter();

    public width: number = window.innerWidth;
    public mutiLang: any = { active: {}, data: [] };
    public closemenu: boolean = false;
    public menuActive: boolean = false;
    activeLogo: boolean = true;

    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public toSlug: ToslugService,
        public routerAtc: ActivatedRoute,
    ) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.globals.initHeader.changeState(event.url)
            }
        })
        this.globals.initHeader.changeState(location.pathname)

        let language = this.globals.language.get(false) !== 0 ? this.globals.language.get(false) : 2;

        this.mutiLang.data = this.globals.language.getData();

        if (this.mutiLang.data.length > 0) {
            this.mutiLang.data.filter(item => {
                if (+item.id == +language) {
                    this.mutiLang.active = item
                }
                return item
            })
        };
    }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.width > 768 ? '' : this.menuMobile.onMenu(true)
            }
        })
    }

    public menuMobile = {
        show: <boolean>true,
        onMenu: (show: boolean) => {
            this.menuMobile.show = show;
            let elm2 = document.getElementById("menu-mobi");
            let headerCenter = document.getElementById("header-center");
            if (this.menuMobile.show == false) {
                elm2.classList.add("menu-mobi-block");
                elm2.classList.remove("menu-mobi-hidden");
                document.querySelector('.menu').classList.add("navbar-fixed-mobile");
                headerCenter.classList.add("navbar-fixed");

            } else {
                elm2.classList.add("menu-mobi-hidden");
                elm2.classList.remove("menu-mobi-block");
                document.querySelector('.menu').classList.remove("navbar-fixed-mobile");
                var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
                if (currentScroll == 0) {
                    headerCenter.classList.remove("navbar-fixed");
                }
            }
        },
    }

    public search = {
        token: '',

        icon: <boolean>false,

        value: '',

        onSearch: () => {

            this.globals.initHeader.isActive = 0

            this.activeLogo = false;

            this.search.value = this.toSlug._ini(this.search.value.replace(/\s+/g, " ").trim());

            if (this.search.check_search(this.search.value)) {

                this.router.navigate(['/' + this.globals.language.getCode() + '/search/' + this.search.value]);

                document.getElementById('search').blur();
            }

            this.search.value = '';

            this.search.icon = false;
        },

        check_search: (value) => {
            let skip = true;
            skip = (value.toString().length > 0 && value != '' && value != '-' && value != '[' && value != ']' && value != '\\' && value != '{' && value != '}') ? true : false;
            return skip;
        },

        showSearch: () => {
            this.search.icon = !this.search.icon;
        }
    }

    onLanguage = (language_id: string | number) => {
        this.mutiLang.data.filter((item: { id: string | number }) => {
            if (+item.id == +language_id) {
                this.mutiLang.active = item;
            }
            return item;
        })

        this.globals.language.set(language_id, false);

        setTimeout(() => {
            this.router.navigate(['/' + this.mutiLang.active.code])
        }, 500);

        this.loading.emit('');
    }

    handleLogoClick = () => {
        const state = this.globals.initHeader
        if (state.state && state.isActive !== 0) {
            state.isActive = 0
            state.state = false
            state.isFixed = false
        }
    }
}

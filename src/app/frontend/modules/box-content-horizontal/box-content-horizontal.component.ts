import { Component, Input } from '@angular/core';
import { Globals } from 'app/globals';

@Component({
    selector: 'box-content-horizontal',
    templateUrl: './box-content-horizontal.component.html',
    styleUrls: ['./box-content-horizontal.component.scss'],
})
export class BoxContentHorizontalComponent {

    @Input('item') item: any;
    @Input('type') type: number = 0;

    constructor(
        public globals: Globals
    ) { }

    ngOnInit() { }

}
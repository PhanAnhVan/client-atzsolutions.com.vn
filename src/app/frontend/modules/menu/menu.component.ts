import {
    Component, OnDestroy,
    OnInit
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Globals } from 'app/globals';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, OnDestroy {
    private connect

    width: number = window.innerWidth
    menu = []
    isHiddenLabel: boolean

    constructor(
        public globals: Globals,
        public router: Router,
        public route: ActivatedRoute
    ) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.globals.initHeader.isActiveLink = event.url.slice(1)
            }
        })
        this.globals.initHeader.isActiveLink = location.pathname.slice(1)

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getMenuMain':
                    this.menu = this.compaid(res.data)
                    break

                default: break
            }
        })
    }

    ngOnInit() {
        this.width > 1024 && this.globals.send({
            path: 'api/getmenu',
            token: 'getMenuMain',
            params: { position: 'menuMain' }
        })
    }

    ngOnDestroy() { this.connect.unsubscribe() }

    compaid(data) {
        let list = [];

        data = data.filter(function (item) {
            let v = isNaN(+item.parent_id) || item.special == 1 ? 0 : +item.parent_id;

            v == 0 ? '' : list.push(item);

            return v == 0 ? true : false;
        });

        let compaidmenu = (data, skip, level = 0) => {
            level = level + 1;

            if (skip == true) {
                return data;
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = [];

                    list = list.filter((item) => {
                        let skip = +item.parent_id == +data[i]['id'] ? false : true;

                        if (skip == false) {
                            obj.push(item);
                        }

                        return skip;
                    });

                    let skip = obj.length == 0 ? true : false;

                    data[i]['level'] = level;

                    data[i]['data'] = compaidmenu(obj, skip, level);
                }

                return data;
            }
        };

        return compaidmenu(data, false);
    }

    _handleRouter = (item: { link: string }) => {

        this.globals.initHeader.isActive = 0

        this.router.navigate(['/' + item.link])

        this.globals.initHeader.isActiveLink = item.link

        this.globals.initHeader.isFixed = false
    }

    routerLink = (item, args: boolean) => {
        this.globals.initHeader.state = true

        if (item.data && item.data.length > 0) {
            item.code === 'resources' || item.code === 'company' ? this.isHiddenLabel = true : this.isHiddenLabel = false

            if (this.globals.initHeader.isActive === +item.id && !args) {
                this.handleCloseBtn()
            } else {
                this.globals.initHeader.isActive = +item.id
                this.globals.initHeader.isFixed = true

                if (args || this.globals.initHeader.isActive === 0) {
                    const CODE_LIST = ['resources', 'company']
                    if (CODE_LIST.includes(item.code)) return

                    this._handleRouter(item)
                }
            }
        } else {
            this._handleRouter(item)
        }
    }

    handleCloseBtn() {
        this.globals.initHeader.isActive = 0
        this.globals.initHeader.isFixed = false
        this.globals.initHeader.changeState(location.pathname)
    }
}
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { Globals } from "../../../globals";

@Component({
    selector: "app-footer",
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit {
    public connect;
    public company;
    public type = "password";

    public width: number = 0;

    // language: string

    private token = {
        menu: 'api/getmenu'
    }
    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router
    ) {
        // if (window['histats']) {
        //     window['histats']();
        // }

        this.width = window.innerWidth;

        // this.language = this.globals.language.getCode().toString()

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'menuFooterAbout':
                    this.about.data = res.data
                    break

                case 'menuFooterProduct':
                    this.product.data = res.data
                    this.product.link = this.product.data[0].link.split('/', 2).toString().replace(',', '/')
                    break

                case 'menuFooterService':
                    this.service.data = res.data
                    this.service.link = this.service.data[0].link.split('/', 2).toString().replace(',', '/')
                    break

                case 'menuFooterResources':
                    this.resources = res.data;
                    break;

                default:
                    break;
            }
        });

        const path = location.pathname.slice(1) || 'en'
        switch (path) {
            case 'vn':
                this.contact.link = path + '/' + 'lien-he'
                break
            default:
                this.contact.link = path + '/' + 'contact'
                break
        }
    }

    ngOnInit() {
        this.globals.send({ path: this.token.menu, token: "menuFooterProduct", params: { position: "product" } });
        this.globals.send({ path: this.token.menu, token: "menuFooterAbout", params: { position: "about" } });
        this.globals.send({ path: this.token.menu, token: "menuFooterService", params: { position: "service" } });
        this.globals.send({ path: this.token.menu, token: 'menuFooterResources', params: { position: 'resources' } })
    }

    ngOnDestroy() { this.connect.unsubscribe() }

    getType = (link, type) => {
        switch (+type) {
            case 1:
            case 2:
            case 3:
            case 4:
                link = link;
                break;

            default:
                break;
        }
        return link;
    }

    service: any = {
        data: [],
        link: ''
    }

    product: any = {
        data: [],
        link: ''
    }

    about: any = { data: [] }

    resources = []

    contact = { link: '' }
}
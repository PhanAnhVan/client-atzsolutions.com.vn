import { Component, Input, OnInit } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
  selector: 'app-box-project',
  templateUrl: './box-project.component.html',
  styleUrls: ['./box-project.component.scss']
})
export class BoxProjectComponent implements OnInit {

  @Input('item') item: any;

  constructor(
    public globals: Globals
  ) { }

  ngOnInit() { }

}

import { Component, Input, } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
    selector: 'box-content-grid',
    templateUrl: './box-content-grid.component.html',
    styleUrls: ['./box-content-grid.component.scss'],

})

export class BoxContentGridComponent {
    @Input('item') item: any;
    constructor(
        public globals: Globals
    ) { }

    ngOnInit() {


    }
}


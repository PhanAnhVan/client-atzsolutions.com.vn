import { Component, HostListener, OnDestroy, OnInit } from '@angular/core'
import { Meta, Title } from '@angular/platform-browser'
import { Router } from '@angular/router'
import { Globals } from 'app/globals'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect
    public width: number = window.innerWidth
    public language: string = this.globals.language.getCode() + '/' || '';

    constructor(
        public globals: Globals,
        public router: Router,
        private title: Title,
        private meta: Meta
    ) {
        this.title.setTitle(this.globals.company.name)
        this.meta.updateTag({
            name: 'description',
            content: this.globals.company.description
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getslide':
                    this.slide.data = res.data;
                    break;

                case 'getTagNavigator':
                    this.tagNavigator.data = res.data;
                    break;

                case 'getProductHome':
                    this.product.data = res.data;
                    break;

                case 'getAboutus':
                    this.about.data = res.data;
                    break;

                case 'getProjectHome':
                    this.project.data = res.data;
                    break;

                case 'getContentHome':
                    this.content.data = res.data
                    break

                case 'getPartner':
                    this.partner.data = res.data;
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.slide.send();
        this.tagNavigator.send();
        this.product.send();
        this.about.send();
        this.content.send();
        this.partner.send();
        this.project.send();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    @HostListener('window: scroll') onScroll = () => {
        const effect = 'categoryEffect linear forwards';
        let position = Math.round(scrollY);
        let category_0 = document.getElementById('category-0'),
            category_1 = document.getElementById('category-1'),
            category_2 = document.getElementById('category-2'),
            category_3 = document.getElementById('category-3');

        if (this.width > 768) {
            position > 75 && category_0 && (category_0.style.animation = `${effect} 1s`)
            position > 175 && category_1 && (category_1.style.animation = `${effect} 1.25s`)
            position > 275 && category_2 && (category_2.style.animation = `${effect} 1.4s`)
            position > 375 && category_3 && (category_3.style.animation = `${effect} 1.6s`)
        }
    }

    public itemLength: number = 0;
    private token = {
        menu: 'api/getmenu',
        product: 'api/home/getproductgroup',
        project: 'api/home/project',
    };

    public slide = {
        token: 'api/home/slide',
        data: [],
        send: () => {
            this.globals.send({
                path: this.slide.token,
                token: 'getslide',
                params: { type: 1 },
            });
        },
        options: {
            loop: true,
            mouseDrag: true,
            touchDrag: false,
            pullDrag: false,
            navSpeed: 500,
            autoplayTimeout: 10000,
            autoplaySpeed: 1000,
            autoplay: false,
            items: 1,
            dots: true,
            nav: true,
            navText: [
                "<img src='../../../../assets/img/left-chevron.png'>",
                "<img src='../../../../assets/img//right-arrow.png'>",
            ],
        },
    };

    public tagNavigator = {
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.menu,
                token: 'getTagNavigator',
                params: { position: 'tagNavigator' },
            });
        },
    };

    public product = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.token.product,
                token: 'getProductHome',
            });
        },
        options: {
            loop: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            autoplay: false,
            margin: 10,
            responsive: {
                0: {
                    items: 2,
                    stagePadding: this.itemLength <= 2 ? 0 : 50,
                },
                415: {
                    items: 3,
                    stagePadding: this.itemLength <= 2 ? 0 : 100,
                },
                940: {
                    items: 5,
                    stagePadding: this.itemLength <= 5 ? 0 : 200,
                },
            },
            dots: false,
            nav: true,
            navText: [
                "<img src='../../../../../assets/img/left-chevron.png'>",
                "<img src='../../../../../assets/img//right-arrow.png'>",
            ],
        },
    };

    public about = {
        token: 'api/home/getAbout',
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.about.token, token: 'getAboutus' });
        },
    };

    public project = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.token.project,
                token: 'getProjectHome',
            });
        },
        options: {
            loop: true,
            autoplaySpeed: 3000,
            autoplayTimeout: 2500,
            stagePadding: this.width > 415 ? 0 : 25,
            autoplay: true,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            nav: false,
            dots: false,
            margin: this.width > 415 ? 30 : 15,
            responsive: {
                0: {
                    items: 1,
                },
                425: {
                    items: 3,
                },
            },
        },
    };

    content = {
        token: 'api/home/content',
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.content.token,
                token: 'getContentHome'
            })
        },
        options: {
            loop: true,
            autoplaySpeed: 3000,
            autoplayTimeout: 2700,
            autoplay: true,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            nav: false,
            dots: false,
            stagePadding: this.width > 415 ? 0 : 25,
            margin: this.width > 415 ? 30 : 15,
            responsive: {
                0: {
                    items: 1
                },
                425: {
                    items: 3
                }
            }
        }
    }

    public partner = {
        token: 'api/home/getPartner',
        data: [],
        send: () => {
            this.globals.send({
                path: this.partner.token,
                token: 'getPartner',
            });
        },
        options: {
            autoWidth: true,
            loop: true,
            autoplay: true,
            autoplaySpeed: 3500,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            responsive: {
                0: {
                    items: 4,
                    margin: 10,
                },
                700: {
                    items: 6,
                    margin: 17.5,
                },
                940: {
                    items: 8,
                    margin: 17.5,
                },
            },
            nav: true,
            dots: false,
            navText: [
                "<img src='../../../../../assets/img/left-chevron.png' width='30px'>",
                "<img src='../../../../../assets/img//right-arrow.png' width='30px'>",
            ],
        },
    };
}
